# sistema-ingreso-cidenet



## Instructivo instalación

1. Descargar ide SpringTools:
 https://download.springsource.com/release/STS4/4.14.0.RELEASE/dist/e4.23/spring-tool-suite-4-4.14.0.RELEASE-e4.23.0-win32.win32.x86_64.self-extracting.jar 
o en la pagina:
https://spring.io/tools

2.Descargar workbench 8.0 CE para gestionar la base de datos.
nota: Desde la aplicacion se encuentra la instruccion para crear la base de datos.(Punto 3)

3.Script base de datos
  
  * En la ruta Src/main/resources en el archivo application.properties se encuentra la configuracion para la creacion de la base de datos.
  cambiar los siguientes valores segun sus credenciales creadas en workbench
  spring.datasource.username = root
  spring.datasource.password = 

  * Para crear la base de datos se usa la siguiente instruccion
    spring.jpa.hibernate.ddl-auto=create 

 *Una vez se crea la base de datos comentarear la linea y habilitar la linea del update.
 spring.jpa.hibernate.ddl-auto=update
 #spring.jpa.hibernate.ddl-auto=create 
    

## FUNCIONALIDAD

Listar, registrar y eliminar empleados.

## FIRMA DEL SERVICIO 
{
    "identificacion":"",
    "tipo_identificacion":"",
    "pmr_nombre":"",
    "sgd_nombre":"",
    "pmr_apellido":"",
    "sgd_apellido":"",
    "area":"",
    "email":"",
    "pais_empleo":"",
    "fechaIngreso":"",
    "fechaRetiro":"",
    "estado":""
} 
## Endpoint para consumir desde Postman

nota: 
1.para buscar y eliminar por id se coloca el valor al final de tipo número.
2. La coleccion la puede sacar de la siguiente ruta dentro del proyecto:
sistema-ingreso-cidenet\gps-empleados\src\main\resources\coleccion-postman

Método: POST
Registrar empleado

http://localhost:8080/api/v1/empleado

formato: json

{
    "identificacion":"1054549308",
    "tipo_identificacion":"C.C",
    "pmr_nombre":"Duvan",
    "sgd_nombre":"Andres",
    "pmr_apellido":"Duque",
    "sgd_apellido":"Diaz",
    "area":"front",
    "email":"duvanduque@cidenet.com.us",
    "pais_empleo":"Colombia",
    "fechaIngreso":"2018-02-27T12:26:30.107",
    "fechaRetiro":"2019-01-27T18:14:01.184",
    "estado":"Activo"
} 

-------------------------------------------
Método: GET
Buscar empleado

http://localhost:8080/api/v1/buscarempleado/
---------------------------------------------

Método: DEL
Eliminar empleado

http://localhost:8080/api/v1/eliminar/

-------------------------------------------

Método: PUT
Actualizar

formato: json

http://localhost:8080/api/v1/actualizar/1
{
    "identificacion":"1054549309",
    "tipo_identificacion":"C.C",
    "pmr_nombre":"luis",
    "sgd_nombre":"julian",
    "pmr_apellido":"Polo",
    "sgd_apellido":"Diaz",
    "area":"publicidad",
    "email":"luispolo@cidenet.com.us",
    "pais_empleo":"Colombia",
    "fechaIngreso":"2018-02-27T12:26:30.107",
    "fechaRetiro":"2019-01-27T18:14:01.184",
    "estado":"Activo"
} 
-------------------------------------------

Listar empleados

http://localhost:8080/api/v1/listados

## Uso Swagger2
En   caso de ejecutar swagger puede usar la siguiente url en su equipo local una vez haya ejecutado la aplicacion con springboot.
http://localhost:8080/api/v1/swagger-ui.html
